import json

with open("offices.json", "r") as file_:
	offices = json.loads(file_.read())

	longest_office = ""

	for office in offices:
		if len(longest_office) < len(office["trade_name"]):
			longest_office = office["trade_name"]

	print(longest_office)
