"""Turn modules into subpackages. (See: Heiwa database and views)"""

import os

for i in os.listdir("."):
	# Utils is a directory
	# No __pycache__, __init__, etc.
	# Don't move this script
	if i == "utils" or i.startswith("__") or i == "packagify.py":
		continue

	os.mkdir(i[:-3])

	os.rename(i, f"{i[:-3]}/__init__.py")
