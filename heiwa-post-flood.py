"""Floods a Heiwa forum with threads."""

import concurrent.futures
import requests
import time

API_URL = "http://localhost:5001/posts"
API_KEY = (
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJIZWl3YSIsImlhdCI6MTY0MzgwMzExOSwiZXhwIjoxNjc1MzYwNzE5LCJzdWIiOiIzM2YwNmFlNi1hZTRmLTQ3ODMtYjNiZi0yZDk4ZDRjMWRiNzEifQ.XmogHPN0pVNKHd1s2m0oHveZpYzwGDO3PtSdDJoH2zI"
)
POST_COUNT = 512
THREAD_IDS = [
	"075f1ac3-0be4-42e3-b5f7-a125a3ef8a8b",
	"a74c7ff5-34ea-4b92-b1da-fa33b83ec160",
	"15b5c2ff-2a78-4b8a-b2a5-a0bffdfba498",
	"1f38917e-7957-406e-a07d-28dfaed3aed5",
	"37e81dd5-f7f8-4ffc-9815-295feb378b3b",
	"d25d67a2-e63f-4cc0-8ae4-14438d2a7061",
	"39f310dc-bc50-4ad5-b66a-3ed34bc94f2c",
	"c1c9a28c-90ca-4db9-87ad-12b5c998ab8b",
	"246ccc0d-cc7a-4167-87fc-33e21d74f605",
	"edd51794-0791-4535-9abf-1d50eb4a16ca",
	"00f34fd0-b716-4787-b4fc-a2cde507398f",
	"b77dfb7d-b54d-4574-bb81-849326694fc5",
	"21e3b5dc-94f6-463a-8610-c769d3ce4ba1",
	"6f28279f-87eb-4a37-81fc-ab49f54c5976",
	"8a2001c2-e143-4fed-9174-14f2ffca4c33",
	"df11c4ba-af64-46a2-8c99-32bb7bde955c",
	"66cb8f73-0e25-4259-adaa-874b5fc2d3dc",
	"dc7cd699-e68c-455d-b1a1-9691ac0bf84e",
	"1001a7f1-0e91-4888-add7-e41a486862d3",
	"e49fa1e3-f9e9-40ea-872d-d0329f7b6ace",
	"58df548c-80bb-4257-9f0d-f194cf258734",
	"c1447233-42b0-4613-aff3-79f878ea9569",
	"7d511f7a-58d9-47dd-adc2-e89740ac92ba",
	"15a2b326-f03d-4ec4-9552-79e18cce9425",
	"26dfe111-59aa-4ccc-ab26-b14273aaa1df",
	"067d9363-b396-420f-8ddc-1f855f906e43",
	"f08cc883-399e-461e-97b5-4f0148a96292",
	"eec50a86-90d1-4ce7-8d39-09df770967cf",
	"5837f554-7826-46e4-aea4-c3ba56b7ba83",
	"b43582fa-999a-4c8c-b0b6-1d8c0b4da7d1",
	"3f23b20c-afd8-4dfe-bc5d-c534d096298a",
	"73566527-c0ae-45f5-a3ab-59b6d728f8e3",
	"4b35b9cb-cefe-4604-9eb9-b8c3f1f84eee",
	"d7f7c936-e9ae-40a7-825f-8fa5b7cbac0f",
	"9c1680cc-13ef-4c8c-9a40-38420e875a38",
	"a48f6f63-939e-42eb-8d78-c33139b28615",
	"acd9954b-7691-4003-9093-9f18d1e73a99",
	"f548ad86-7fb5-4d36-816a-7dc0ee7168fd",
	"dbdeeaf2-19ab-4520-844b-ff5fe2f5dc8d",
	"2785f842-ec26-4ffc-885a-3404bb86cb5d",
	"a7993cb3-f50a-4d75-8f1c-0a3d3d6e17ef",
	"0338f3f4-da08-4e5a-af94-e77986325e23",
	"d497a37b-fe9c-46ff-8d0c-87ed5b136ff9",
	"08db28da-a900-486e-bef5-e502aa900d72",
	"0fdceccb-4b9b-416a-a5c3-f5e8481216b0",
	"a93152ff-24f0-4bc6-abbf-112550f32b31",
	"2f286b05-1cac-47c9-83f6-3939e73e5309",
	"e77d3fbb-4ebd-40c9-a83c-fbf12e9d7095",
	"4c32013e-56d1-4074-87b9-19b5221a07a0",
	"cae1e02b-a5b4-4b0e-9989-2c3b410f720d"
]

def create_post(position):
	thread_id_length = len(THREAD_IDS)

	thread_id_position = position

	# Bad solution, but it works... usually!
	if position > thread_id_length:
		while True:
			thread_id_position -= thread_id_length

			if thread_id_position <= thread_id_length:
				break

	thread_id_position -= 1

	response = requests.post(
		API_URL,
		headers={
			"Authorization": f"Bearer {API_KEY}"
		},
		json={
			"thread_id": THREAD_IDS[thread_id_position],
			"subject": f"Re: A thread. Response number {position}",
			"content": f"This post contains... a {position}. I don't know."
		}
	)

	if not response.ok:
		return "Error: " + str(response.json())

futures = []

with concurrent.futures.ThreadPoolExecutor(max_workers=250) as executor:
	for i in range(0, POST_COUNT):
		futures.append(executor.submit(create_post, position=i))

	for future in futures:
		result = future.result()

		if result is not None:
			print(result)
