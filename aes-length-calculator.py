"""Calculate the length of an AES-CBC encrypted string.
Requires PyCryptodome.
"""

import base64
import Crypto.Cipher.AES
import Crypto.Util.Padding

PLAINTEXT_FILE = ""

if PLAINTEXT_FILE != "":
	with open(PLAINTEXT_FILE, "rb") as f:
		plaintext = f.read()
else:
	plaintext = input("Enter plaintext: ").encode("utf-8")

KEY = b"sixteencharacter"  # 16 bytes

cipher = Crypto.Cipher.AES.new(KEY, Crypto.Cipher.AES.MODE_CBC)
# If the keys aren't random like they should be in Heiwa,
# also introduce IVs

ciphertext = cipher.encrypt(
	Crypto.Util.Padding.pad(
		plaintext,
		Crypto.Cipher.AES.block_size
	)
)
ciphertext_b64 = base64.b64encode(ciphertext).decode("utf-8")

print(f"""
Ciphertext:
	Raw (python format): {ciphertext}
	Raw (hex): {ciphertext.hex()}
	Base64: {ciphertext_b64}
	
	__len__ (raw): {len(ciphertext)}
	__len__ (base64): {len(ciphertext_b64)}""")
