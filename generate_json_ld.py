import json

import sqlalchemy
import sqlalchemy.orm

from heiwa import database

write = True

for obj in database.__all__:
	actual_object = getattr(database, obj)

	print(f"# Trying: {actual_object}")

	if obj == "Base" or not isinstance(actual_object, sqlalchemy.orm.DeclarativeMeta):
		continue

	json_ld = {}


	for column_set in sqlalchemy.inspect(actual_object).mapper.column_attrs:
		column_type = column_set.columns[0].type

		if isinstance(
			column_type,
			sqlalchemy.Integer | sqlalchemy.BigInteger
		):
			json_ld[column_set.key] = "https://schema.org/Number"
		elif isinstance(
			column_type,
			sqlalchemy.String
		):
			json_ld[column_set.key] = "https://schema.org/Text"
		elif isinstance(
			column_type,
			database.utils.UUID
		):
			json_ld[column_set.key] = "https://schema.org/identifier"
		elif isinstance(
			column_type,
			sqlalchemy.DateTime
		):
			json_ld[column_set.key] = "https://schema.org/DateTime"
		elif isinstance(
			column_type,
			sqlalchemy.Boolean
		):
			json_ld[column_set.key] = "https://schema.org/Boolean"
		else:
			json_ld[column_set.key] = f"FIXME - {column_type}"

	dumped_str = json.dumps(
		json_ld,
		indent=4
	)

	print(dumped_str)

	if write:
		with open(f"contexts/{obj}.jsonld", "w") as file_:
			file_.write(dumped_str)
