"""A utility for testing the speeds of different endpoints in the Heiwa API."""

import datetime
import decimal
import os
import time

import requests

FILENAME = "results.csv"  # Where results go
MAX_LIMIT = 512
API_KEY = (
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJIZWl3YSIsImlhdCI6MTY0MzgwMzExOSwiZXhwIjoxNjc1MzYwNzE5LCJzdWIiOiIzM2YwNmFlNi1hZTRmLTQ3ODMtYjNiZi0yZDk4ZDRjMWRiNzEifQ.XmogHPN0pVNKHd1s2m0oHveZpYzwGDO3PtSdDJoH2zI"
)
ENDPOINT = "http://localhost:5001/posts"
# The time to wait between each request, in seconds
WAIT_BETWEEN_REQUESTS = 0.05
TRIES = 5

speeds = {}

for current_try in range(1, TRIES + 1):
	for current_limit in range(1, MAX_LIMIT + 1):  # Include the last number
		time_before_request = datetime.datetime.now()

		response = requests.get(
			ENDPOINT,
			json={
				"limit": current_limit,
				"offset": 0
			},
			headers={
				"Authorization": f"Bearer {API_KEY}"
			}
		)

		difference = datetime.datetime.now() - time_before_request

		# No floating point errors this time
		difference_in_ms = (
			difference.seconds * 1000
			+ decimal.Decimal(difference.microseconds) / 1000
		)

		speeds[current_limit] = difference_in_ms

		if not response.ok:
			print("Error - " + str(response.json()))

		print(f"{current_try} - {current_limit}: {difference_in_ms}")

		# Delete parsed permissions each time, bad yet reliable solution
		# Run scipt as the postgres user
		#os.system("psql --dbname heiwa --command \"delete from forum_parsed_permissions;\"")
		# (only used for forums)

		time.sleep(WAIT_BETWEEN_REQUESTS)

	with open(f"{current_try}-{FILENAME}", "w") as f:
		f.write("Set limit,Response time\n")

		for limit, response_time in speeds.items():
			f.write(f"{limit},{response_time}\n")
