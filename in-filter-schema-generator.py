"""Generates a Heiwa ``$in`` forum permission filter schema.
WARNING: Outdated. Works, but needless repeated code.
"""


ATTRIBUTES = [
	"category_create",
	"category_delete",
	"category_edit",
	"category_move",
	"category_view",
	"forum_create",
	"forum_delete",
	"forum_edit",
	"forum_merge",
	"forum_move",
	"forum_view",
	"post_create",
	"post_delete_own",
	"post_delete_any",
	"post_edit_own",
	"post_edit_any",
	"post_edit_vote",
	"post_move_own",
	"post_move_any",
	"post_view",
	"thread_create_approved",
	"thread_create_unapproved",
	"thread_delete_own",
	"thread_delete_any",
	"thread_edit_own",
	"thread_edit_any",
	"thread_edit_approval",
	"thread_edit_lock_own",
	"thread_edit_lock_any",
	"thread_edit_pin",
	"thread_edit_vote",
	"thread_merge_own",
	"thread_merge_any",
	"thread_move_own",
	"thread_move_any",
	"thread_view"
]


for attr in ATTRIBUTES:
	print(f"""\"{attr}\": {{
	"type": "list",
	"schema": PERMISSION_ATTR_SCHEMAS_PARTIAL[\"{attr}\"],
	"minlength": 2,
	"maxlength": max_list_length
}},""")
