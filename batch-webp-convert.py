"""Converts all PNGs in a folder to WEBPs."""

import os

for file in os.listdir("."):
	if not file.endswith(".png"):
		continue

	os.system(f"cwebp -q 60 {file} -o {file.replace('.png', '.webp')}")
