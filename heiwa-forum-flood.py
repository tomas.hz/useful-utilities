"""Floods a Heiwa instance with forums."""

import concurrent.futures
import requests
import time

API_URL = "http://localhost:5001/forums"
API_KEY = (
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJIZWl3YSIsImlhdCI6MTY0MzgwMzExOSwiZXhwIjoxNjc1MzYwNzE5LCJzdWIiOiIzM2YwNmFlNi1hZTRmLTQ3ODMtYjNiZi0yZDk4ZDRjMWRiNzEifQ.XmogHPN0pVNKHd1s2m0oHveZpYzwGDO3PtSdDJoH2zI"
)
FORUM_COUNT = 50

def create_thread(return_id: bool = True):
	response = requests.post(
		API_URL,
		headers={
			"Authorization": f"Bearer {API_KEY}"
		},
		json={
			"parent_forum_id": None,
			"category_id": None,
			"order": 0,
			"name": "Hello world",
			"description": "Hello world"
		}
	)

	if not response.ok:
		return "Error: " + str(response.json())

	if return_id:
		return response.json()["id"]

futures = []

with concurrent.futures.ThreadPoolExecutor(max_workers=250) as executor:
	for i in range(0, FORUM_COUNT):
		futures.append(executor.submit(create_thread))

	for future in futures:
		result = future.result()

		if result is not None:
			print(result)
