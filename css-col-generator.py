"""WARNING: THERE IS USUALLY NO GOOD REASON TO USE THIS! SASS is much more
useful and extensible. I just don't feel like learning it at the moment.
"""

col_count = 20

infix = "xxl-"

for i in range(1, col_count + 1):
	print(f""".col-{infix}{i} {{
	width: {((100 / col_count) * i) - 1}%;
	margin-left: 0.5%;
	margin-right: 0.5%;
}}""")
