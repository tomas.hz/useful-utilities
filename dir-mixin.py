class DirMixin:
	"""A mixin which adds a method to print all of the mixed-in class' attributes,
	like methods and variables, as well as their values.
	"""

	def dir(
		self,
		public_only: bool = True
	) -> None:
		print(f"""---
{self}:
""")

		for attr in dir(self):
			if attr.startswith("_") and public_only:
				continue

			print(f"\t{attr}: {getattr(self, attr)}")

		print("\n---")


if __name__ == "__main__":
	# Example

	import requests

	class Session(requests.Session, DirMixin):
		pass

	session = Session()
	session.dir()
