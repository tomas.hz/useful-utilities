"""A utility for testing the speeds of different creation endpoints in the
Heiwa API.
"""

import datetime
import decimal
import os
import time

import requests

FILENAME = "results.csv"  # Where results go
API_KEY = (
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJIZWl3YSIsImlhdCI6MTY0MzgwMzExOSwiZXhwIjoxNjc1MzYwNzE5LCJzdWIiOiIzM2YwNmFlNi1hZTRmLTQ3ODMtYjNiZi0yZDk4ZDRjMWRiNzEifQ.XmogHPN0pVNKHd1s2m0oHveZpYzwGDO3PtSdDJoH2zI"
)
ENDPOINT = "http://localhost:5001/threads"
# The time to wait between each request, in seconds
WAIT_BETWEEN_REQUESTS = 0.05
TRIES = 512

speeds = []

for current_try in range(1, TRIES + 1):
	time_before_request = datetime.datetime.now()

	requests.post(
		ENDPOINT,
		json={
			"content": "Hello world.",
			"forum_id": "67f745ec-1293-41fb-a7d1-b472da3d20ff",
			"is_locked": False,
			"is_approved": True,
			"is_pinned": False,
			"tags": [],
			"name": "Hello world."
		},
		headers={
			"Authorization": f"Bearer {API_KEY}"
		}
	)

	difference = datetime.datetime.now() - time_before_request

	# No floating point errors this time
	difference_in_ms = (
		difference.seconds * 1000
		+ decimal.Decimal(difference.microseconds) / 1000
	)

	speeds.append(difference_in_ms)

	print(f"{current_try}: {difference_in_ms}")

	# Delete parsed permissions each time, bad yet reliable solution
	# Run scipt as the postgres user
	#os.system("psql --dbname heiwa --command \"delete from forum_parsed_permissions;\"")
	# (only used for forums)

	time.sleep(WAIT_BETWEEN_REQUESTS)

with open(f"{FILENAME}", "w") as f:
	for response_time in speeds:
		f.write(f"{response_time}\n")
