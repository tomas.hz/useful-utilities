import json

with open("offices.json", "r") as file_:
	offices = json.loads(file_.read())

	longest_office = ""

	for office in offices:
		if len(longest_office) < len(office["name"]):
			longest_office = office["name"]

	print(longest_office)
