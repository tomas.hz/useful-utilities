import json
import os

CAPITALIZATION_MAPPING = {
	"á": "Á",
	"č": "Č",
	"ď": "Ď",
	"é": "É",
	"ě": "Ě",
	"í": "Í",
	"ň": "Ň",
	"ó": "Ó",
	"ř": "Ř",
	"š": "Š",
	"ť": "Ť",
	"ú": "Ú",
	"ů": "Ů",
	"ý": "Ý",
	"ž": "Ž"
}

def capitalize(string: str) -> str:
	final_string = ""

	for word in string.split(" "):
		if len(word) == 0:
			continue

		if word in (
			"u",
			"nad",
			"pod",
			"z",
			"k"
		) or len(word) == 1:
			word = word.lower()
			final_string += word + " "

			continue

		word = list(word)

		word[0] = (
			CAPITALIZATION_MAPPING[word[0]] if word[0] in CAPITALIZATION_MAPPING
			else word[0].upper()
		)

		word = "".join(word)

		final_string += word + " "

	final_string = final_string[:-1]

	return final_string

with open("offices_raw_data.json", "r") as raw_office_file:
	source_offices = json.loads(raw_office_file.read())
	actual_offices = []

	for possible_office in source_offices["list"]["box"]:
		trade_name = capitalize(possible_office["name"]["tradeName"])

		if not (
			trade_name.startswith("Město ")
			or trade_name.startswith("Statutární Město ")
			or trade_name.startswith("Obec ")
			or trade_name.startswith("Městská Část ")
			or trade_name.startswith("Městys ")
		):
			continue

		zip_ = list(
			str(possible_office["address"]["zip"])
		)

		zip_.insert(3, " ")

		zip_ = "".join(zip_)

		office = {
			"name": trade_name,
			"address": {
				"street": (
					possible_office["address"]["street"]
					+ (
						" "
						if (
							possible_office["address"]["street"] != ""
							and possible_office["address"]["street"] is not None
						)
						else ""
					)
					+ str(possible_office["address"]["cp"])
				),
				"zip": zip_,
				"city": possible_office["address"]["city"],
				"district": possible_office["address"]["district"]
			},
			"ds_id": possible_office["id"]
		}

		print(office)

		actual_offices.append(office)

	with open(
		"offices.json",
		"w"
	) as finished_office_file:
		finished_office_file.write(json.dumps(actual_offices))
