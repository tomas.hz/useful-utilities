import json

with open("offices.json", "r") as offices_file:
	offices = json.loads(offices_file.read())

	for position, office in enumerate(offices):
		office["name"] = office["trade_name"]

		office.pop("trade_name", None)

with open("offices.json", "w") as offices_file:
	offices_file.write(json.dumps(
		offices,
		indent=4
	))
