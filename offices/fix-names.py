import json

with open("offices.json", "r") as office_file:
	offices = json.loads(office_file.read())

	for position, office in enumerate(offices):
		for string_to_replace in (
			"Obec ",
			"Město ",
			"Městská Část ",
			"Městys ",
			"Statutární Město "
		):
			if office["name"].startswith(string_to_replace):
				new_name = office["name"].replace(string_to_replace, "")

				print(position, office["name"], "->", new_name)

				office["name"] = new_name

				offices[position] = office

with open("offices.json", "w") as office_file:
	office_file.write(
		json.dumps(
			offices,
			indent=4
		)
	)
