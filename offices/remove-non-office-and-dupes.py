import json

bad_name_contains = [
	"Policie"
]

possibly_bad_name_contains = [
	"("
]

with open("offices.json", "r") as offices_file:
	offices = json.loads(offices_file.read())

	#known_office_names = []
	removed_count = 0

	for position, office in enumerate(offices):
		for bad_name in [
			*bad_name_contains,
			*possibly_bad_name_contains
		]:
			if bad_name in office["name"]:
				if (
					input(f"Remove {office['name']}? (y/n) ") == "y"
					or bad_name in bad_name_contains
				):
					offices.pop(position)
					removed_count += 1
					break

		#if office["name"] in known_office_names:
			#print(f"Entirely removing duplicated {office['name']}")

			#for position_, office_ in enumerate(offices):
				#if office_["name"] == office["name"]:
					#offices.pop(position_)

			#removed_count += 1
		#else:
			#known_office_names.append(office["name"])

print("Removed", removed_count)

with open("offices.json", "w") as offices_file:
	offices_file.write(
		json.dumps(
			offices,
			indent=4
		)
	)
