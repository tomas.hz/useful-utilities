import json

DECAPITALIZATION_MAPPING = {
	"Á": "á",
	"Č": "č",
	"Ď": "ď",
	"É": "é",
	"Ě": "ě",
	"Í": "í",
	"Ň": "ň",
	"Ó": "ó",
	"Ř": "ř",
	"Š": "š",
	"Ť": "ť",
	"Ú": "ú",
	"Ů": "ů",
	"Ý": "ý",
	"Ž": "ž"
}

def lower(source: str) -> str:
	letters = list(source)

	for position, letter in enumerate(letters):
		if letter in DECAPITALIZATION_MAPPING:
			letter = DECAPITALIZATION_MAPPING[letter]
		else:
			letter = letter.lower()

		letters[position] = letter

	return "".join(letters)

with open("offices.json", "r") as file_:
	offices = json.loads(file_.read())

	known_name_zips = {}
	repeat_count = 0

	for office in offices:
		name = lower(office["name"])

		if name in known_name_zips and known_name_zips[name] == office["address"]["zip"][:3]:
			print("Found possible dupe:", name)

		known_name_zips[name] = office["address"]["zip"][:3]
