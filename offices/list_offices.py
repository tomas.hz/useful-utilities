import json

with open("offices.json", "r") as offices_file:
	offices = json.loads(offices_file.read())

	for position, office in enumerate(offices):
		print(position, office["name"])
