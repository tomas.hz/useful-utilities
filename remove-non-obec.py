import json

with open("offices.json", "r") as offices_file:
	offices = json.loads(offices_file.read())

	known_office_names = []
	removed_count = 0

	for position, office in enumerate(offices):
		if "Policie" in office["trade_name"]:
			if input(f"Remove {office['trade_name']}? (y/n) ") == "y":
				offices.pop(position)
				removed_count += 1

		if office["trade_name"] in known_office_names:
			print(f"Removing duplicate {office['trade_name']}")

			removed_count += 1
			offices.pop(position)
		else:
			known_office_names.append(office["trade_name"])

print("Removed", removed_count)

with open("offices.json", "w") as offices_file:
	offices_file.write(
		json.dumps(
			offices,
			indent=4
		)
	)
