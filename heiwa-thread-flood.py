"""Floods a Heiwa forum with threads."""

import concurrent.futures
import requests
import time

API_URL = "http://localhost:5001/threads"
API_KEY = (
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJIZWl3YSIsImlhdCI6MTY0MzgwMzExOSwiZXhwIjoxNjc1MzYwNzE5LCJzdWIiOiIzM2YwNmFlNi1hZTRmLTQ3ODMtYjNiZi0yZDk4ZDRjMWRiNzEifQ.XmogHPN0pVNKHd1s2m0oHveZpYzwGDO3PtSdDJoH2zI"
)
THREAD_COUNT = 50
FORUM_IDS = [
	"7724ab97-4bea-4e11-bd14-fa259e3109c0",
	"963a712b-a06e-4067-9db2-d009c28b4632",
	"17806a7b-9901-406a-bff6-67c47b6d4b74",
	"d5e80f6b-d160-4228-b306-f940acf875ec",
	"1173f9c6-f0dc-4d05-bc5f-c687fe0a775b",
	"bb21d19d-6d57-45f5-b150-3a54607b3966",
	"18101cc7-abdf-4c40-a8df-1269a2cf7879",
	"6273bee1-d09a-426c-b864-5354b95dc2c4",
	"5abb4e46-c538-4a34-abc3-135a8ed69ea8",
	"7b6678f5-7bcf-4ff9-aa99-6e8440ee7a91",
	"a5f21e07-a899-405b-98ca-3ac406e63b4e",
	"8346eb75-a5b1-4aa5-8569-c588f9cadc24",
	"d12ec4a1-9e71-43d3-ba17-bdfdbfd4bca5",
	"d82f36d4-3ef0-47cd-9035-49ca37226863",
	"16d0189c-c495-482e-a2dc-2618fa55cc5f",
	"f35f4344-01ed-4d6f-a075-0c0fba1a6530",
	"ec95aaaa-2754-4ede-b486-61d925153abf",
	"ca194354-f669-4a36-8a62-ee9120520ceb",
	"501e7b8a-e979-419e-86cb-848ecc87b51e",
	"6844536e-f715-4276-a046-a1fbe4ef6281",
	"d9e55bfe-ddef-4d92-b661-6bff3d90adc2",
	"d2298d4d-2fd3-453d-858f-1a1024bb7623",
	"fffb62da-48f9-4f4a-937d-3653f52cdb9b",
	"de6cd363-b39a-48b4-88b7-caf0249ce893",
	"dbfb60ba-e0a5-4f09-9151-f936933dccdc",
	"e8a3384f-e0bf-4a59-9d7a-68f97c495660",
	"49cc8dbd-3d80-4767-8ebc-faf3326726dc",
	"40c2b90b-7972-46a3-b7b9-eb8cfe6905c9",
	"361a034a-c920-4c5d-b606-5f58477246ec",
	"44356588-8038-4828-a5cf-51cb19901e6c",
	"8bdaf1fa-16a6-4895-bcde-787bc58b661d",
	"730a9921-ac12-4eb0-a30f-04c14c3d4380",
	"7d3e4cd5-86c6-45de-8692-3ca9d65a660d",
	"3d508569-623c-4499-be1c-7553837915f6",
	"3e3a06da-fed4-441a-858e-155902d17d09",
	"af1465ba-1a80-412f-8443-f7cc0612dd4e",
	"8a8bff62-88c0-4de3-ac40-10520ea0a3de",
	"70193f06-a2af-4b8f-8886-833bb4a78731",
	"74fee14b-2475-4c85-ab7e-f7f3e5125d61",
	"8ee10d6a-e070-4280-b114-39c563d5f1d9",
	"2d632118-4ffc-4465-9302-c0ed5d1cbbfb",
	"213cd07c-6c19-4ebd-9842-2a1965f2c6e6",
	"3b70d5fd-93a0-445b-9643-a869805c2dd0",
	"31c56cff-487e-4213-a47d-f3eba37bd82a",
	"eee49854-1d99-42f9-bf6a-fa94ff599a7a",
	"e392b7e1-bf1f-4cd3-b22a-857754dd936d",
	"8d1c1ac9-b98b-46a8-b2e3-8163baac1d4b",
	"beb6e3d6-fd34-4d4b-bf70-d0800ec5ceb9",
	"a5e2284b-d7d2-4b4c-903d-b7bf5b6a050a",
	"eb96692a-8820-450d-948c-7634b2e3251d"
]

def create_thread(position, return_ids: bool = True):
	forum_id_length = len(FORUM_IDS)

	forum_id_position = position

	# Bad solution, but it works... usually!
	if position > forum_id_length:
		while True:
			forum_id_position -= forum_id_length

			if forum_id_position <= forum_id_length:
				break

	forum_id_position -= 1

	response = requests.post(
		API_URL,
		headers={
			"Authorization": f"Bearer {API_KEY}"
		},
		json={
			"forum_id": FORUM_IDS[forum_id_position],
			"is_approved": True,
			"is_locked": False,
			"is_pinned": False,
			"tags": [],
			"name": f"This is thread number {position}.",
			"content": f"This thread contains exactly {position} {position}s."
		}
	)

	if not response.ok:
		return "Error: " + str(response.json())

	if return_ids:
		return response.json()["id"]

futures = []

with concurrent.futures.ThreadPoolExecutor(max_workers=250) as executor:
	for i in range(0, THREAD_COUNT):
		futures.append(executor.submit(create_thread, position=i))

	for future in futures:
		result = future.result()

		if result is not None:
			print(result)
