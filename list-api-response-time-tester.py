"""A utility for testing the speeds of different listing endpoints in the
Heiwa API.
"""

import datetime
import decimal
import os
import time

import requests

FILENAME = "results.csv"  # Where results go
MAX_LIMIT = 512
API_KEY = (
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJIZWl3YSIsImlhdCI6MTY0MzgwMzExOSwiZXhwIjoxNjc1MzYwNzE5LCJzdWIiOiIzM2YwNmFlNi1hZTRmLTQ3ODMtYjNiZi0yZDk4ZDRjMWRiNzEifQ.XmogHPN0pVNKHd1s2m0oHveZpYzwGDO3PtSdDJoH2zI"
)
ENDPOINT = "http://localhost:5001/posts"
# The time to wait between each request, in seconds
WAIT_BETWEEN_REQUESTS = 0.1
TRIES = 5

speeds = {}

for current_try in range(1, TRIES + 1):
	speeds[current_try] = {}

	for current_limit in range(1, MAX_LIMIT + 1):  # Include the last number
		time_before_request = datetime.datetime.now()

		requests.get(
			ENDPOINT,
			json={
				"limit": current_limit,
				"offset": 0
			},
			headers={
				"Authorization": f"Bearer {API_KEY}"
			}
		)

		difference = datetime.datetime.now() - time_before_request

		# No floating point errors this time
		difference_in_ms = (
			difference.seconds * 1000
			+ decimal.Decimal(difference.microseconds) / 1000
		)

		speeds[current_try][current_limit] = difference_in_ms

		print(f"{current_try} - {current_limit}: {difference_in_ms}")

		# Delete parsed permissions each time, bad yet reliable solution
		# Run scipt as the postgres user
		# os.system("psql --dbname heiwa --command \"delete from forum_parsed_permissions;\"")
		# (only used for forums)

		time.sleep(WAIT_BETWEEN_REQUESTS)

with open(FILENAME, "w") as f:
	f.write(f",Times{',' * TRIES}\n")
	f.write(f",Iteration{',' * TRIES}Average\n")
	f.write("Result count,")

	for attempt in range(1, TRIES + 1):
		f.write(f"{attempt}")

		if attempt != TRIES:
			f.write(",")

	f.write("\n")

	for limit in range(1, MAX_LIMIT + 1):
		f.write(f"{limit},")

		total = 0

		for attempt in range(1, TRIES + 1):
			f.write(f"{speeds[attempt][limit]},")

			total += speeds[attempt][limit]

		f.write(f"{total / TRIES}\n")
