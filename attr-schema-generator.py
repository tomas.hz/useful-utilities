"""Generates basic ATTR_SCHEMAS."""

import json

import sqlalchemy

from heiwa.database import UserBan as model
from heiwa.database.utils import UUID


columns = sqlalchemy.inspect(model).mapper.column_attrs

attr_schemas = {}

for column in columns:
	column_type = column.columns[0].type

	attr_schemas[column.key] = {
		"type": (
			"uuid"
			if isinstance(column_type, UUID) else
			"string"
			if isinstance(column_type, sqlalchemy.String) else
			"integer"
			if isinstance(column_type, sqlalchemy.Integer | sqlalchemy.BigInteger) else
			"datetime"
			if isinstance(column_type, sqlalchemy.DateTime) else
			"boolean"
			if isinstance(column_type, sqlalchemy.Boolean) else
			"FIXME"
		)
	}

	if isinstance(column_type, sqlalchemy.String):
		attr_schemas[column.key].update({
			"minlength": 1,
			"maxlength": f"FIXME database.{model.__name__}.{column.key}.property.columns[0].type.length"
		})

	if isinstance(column_type, sqlalchemy.Integer | sqlalchemy.BigInteger):
		if column.key == "vote_value":
			min_ = "FIXME -constants.BIG_INTEGER_LIMIT"
		else:
			min_ = 0

		attr_schemas[column.key].update({
			"min": min_,
			"max": "FIXME constants.BIG_INTEGER_LIMIT"
		})

	if isinstance(column_type, UUID):
		attr_schemas[column.key]["coerce"] = "convert_to_uuid"

	if isinstance(column_type, sqlalchemy.DateTime):
		attr_schemas[column.key]["coerce"] = "convert_to_datetime"

print("ATTR_SCHEMAS = " + json.dumps(attr_schemas, indent="\t"))
